const gulp = require('gulp'),
      nunjucksRender = require('gulp-nunjucks-render'),
      rename = require('gulp-rename'),
      browserSync = require("browser-sync").create(),
      w3cjs = require('gulp-w3cjs');
      access = require('gulp-accessibility');
      

//Load Previews on Browser on dev
function livePreview(done) {
  browserSync.init({
    server: {
      baseDir: "./dist",
    },
    startPath: "/",
    port: 3001,
  });
  done();
}

// Triggers Browser reload
function previewReload(done) {
  console.log("\n\t Reloading Browser Preview.\n");
  browserSync.reload();
  done();
}

gulp.task('validate', gulp.series(function () {
    return gulp.src('./dist/hpb/ca/fr/*.html')
      .pipe(w3cjs())
      .pipe(w3cjs.reporter());
}));

gulp.task('accessibility', gulp.series(function() {
  return gulp.src('./dist/hpb/ca/fr/*.html')
    .pipe(access({
      force: true
    }))
    .on('error', console.log)
    .pipe(access.report({reportType: 'txt'}))
    .pipe(rename({
      extname: '.txt'
    }))
    .pipe(gulp.dest('reports/txt'));
}));

gulp.task('nunjucks-en', gulp.series(function() {
    // Gets .html and .nunjucks files in pages
    return gulp.src('src/en/pages/**/*.+(html|nunjucks)')
    // Renders template with nunjucks
    .pipe(nunjucksRender({
        path: ['src/en/templates']
    }))
    // output files in app folder
    .pipe(gulp.dest('dist/en'))
}));

gulp.task('nunjucks-fr', gulp.series(function() {
    // Gets .html and .nunjucks files in pages
    return gulp.src('src/fr/pages/**/*.+(html|nunjucks)')
    // Renders template with nunjucks
    .pipe(nunjucksRender({
        path: ['src/fr/templates']
    }))
    // output files in app folder
    .pipe(gulp.dest('dist/fr'))
}));

gulp.task('copy-redirect', gulp.series(function() {
    return gulp.src('./src/index.html').pipe(rename({ basename: 'index'}))
    .pipe(gulp.dest('./dist'));
}));

gulp.task('copy-css-en', gulp.series(function() {
    return gulp.src('./src/assets/**/*')
        .pipe(gulp.dest('./dist/assets'));
}));

gulp.task('copy-js-en', gulp.series(function() {
    return gulp.src('./src/js/**/*')
        .pipe(gulp.dest('./dist/hpb/ca/en/js'));
}));

gulp.task('copy-images-en', gulp.series(function() {
    return gulp.src('./src/images/**/*')
        .pipe(gulp.dest('./dist/hpb/ca/en/images'));
}));

gulp.task('copy-css-fr', gulp.series(function() {
    return gulp.src('./src/css/**/*')
        .pipe(gulp.dest('./dist/hpb/ca/fr/css'));
}));

gulp.task('copy-js-fr', gulp.series(function() {
    return gulp.src('./src/js/**/*')
        .pipe(gulp.dest('./dist/hpb/ca/fr/js'));
}));

gulp.task('copy-images-fr', gulp.series(function() {
    return gulp.src('./src/images/**/*')
        .pipe(gulp.dest('./dist/hpb/ca/fr/images'));
}));

gulp.task('copy-fonts', gulp.series(function() {
    return gulp.src('./src/fonts/**/*')
        .pipe(gulp.dest('./dist/fonts'));
}));

gulp.task('compose', gulp.series(['nunjucks-en', 'nunjucks-fr', 'copy-js-en', 'copy-css-en', 'copy-images-en', 'copy-js-fr', 'copy-css-fr', 'copy-images-fr', 'copy-fonts' , 'copy-redirect']));

gulp.task('watch', gulp.series(function(){
    return gulp.watch('src/**/*.+(html|nunjucks|njk|yaml|json|css|scss)', gulp.series(['compose', previewReload]));
}));

gulp.task('default', gulp.series([livePreview, 'watch']));
gulp.task('prod', gulp.series(['compose']));
