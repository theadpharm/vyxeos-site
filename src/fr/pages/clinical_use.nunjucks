{% extends "layout.nunjucks" %} {% block title %} Usage clinique | Vyxeos {% endblock %} {% block content %}

<style>
    h3.usage{
        font-weight: bold;
        text-decoration: underline;
    }
    .alert-clinical { 
        border: 1px black solid;
    }
    .serious {
        font-weight: bold;
        text-decoration: underline;
    }
</style>



<div class="container">
    <div class="row">
        <div class="col-6">
            <h3 class="usage">Usage clinique :</h3>
            <p><strong>Enfants (&lt;18 ans) :</strong> L’innocuité et l’efficacité de VYXEOS dans le traitement de
la LMA-t et de la LMA-CM nouvellement diagnostiquées n’ont pas été établies
chez les enfants et les adolescents de moins de 18 ans.</p>
            <p><strong>Personnes âgées (&ge;65 ans) :</strong> Aucune différence significative relative à
l’innocuité n’a été observée chez les patients âgés de 65 ans ou plus.</p>

            <div class="alert alert-clinical" role="alert">
                <span class="serious">Mises en garde et précautions importantes :</span> Les recommandations
posologiques de VYXEOS sont différentes de celles du chlorhydrate de
daunorubicine pour injection, de la cytarabine pour injection, du citrate de
daunorubicine liposomal pour injection et de la cytarabine liposomale pour
injection. Vérifiez le nom du médicament et la dose avant la préparation et
l’administration afin d’éviter toute erreur de posologie.
            </div>
            <h3 class="usage">Mises en garde et précautions pertinentes :</h3>
            <ul>
                <li>VYXEOS ne doit pas être remplacé par d’autres produits contenant de la
daunorubicine et (ou) de la cytarabine ni être substitué à de tels produits</li>
                <li>Nécrose tissulaire</li>
                <li>La cardiotoxicité est un risque connu du traitement par les anthracyclines</li>
                <li>Conduite de véhicules et utilisation de machines</li>
                <li>VYXEOS ne doit être administré aux patients ayant des antécédents de
maladie de Wilson ou d’autre maladie liée au cuivre que si les bienfaits du
traitement l’emportent sur les risques. Le traitement par VYXEOS doit être
arrêté chez les patients présentant des signes ou symptômes de toxicité
aiguë du cuivre</li>
                <li>Mucosite gastro-intestinale et diarrhée</li>
                <li>Hématologie : Une myélosuppression sévère entraînant des infections
et des hémorragies mortelles a été signalée chez des patients après
l’administration de VYXEOS. Des numérations globulaires doivent être
réalisées régulièrement pendant le traitement par VYXEOS et des mesures
de soutien appropriées doivent être instaurées en cas de survenue de
complications cliniques liées à une myélosuppression</li>
                <li>L’insuffisance hépatique ou rénale peut accroître le risque de toxicité
associée à la daunorubicine et à la cytarabine</li>
                <li>Des réactions d’hypersensibilité graves, y compris des réactions
anaphylactiques, ont été signalées en association avec la daunorubicine et
la cytarabine</li>
               
            </ul>

        </div>
        <div class="col-6">
            <ul>
                <li>Sensibilité accrue aux infections</li>
                <li>La fonction cardiaque et l’uricémie doivent être étroitement surveillées.
Un traitement approprié doit être instauré en cas d’apparition d’une
hyperuricémie</li>
                <li>Grossesse : On ne dispose d’aucune donnée sur l’utilisation de VYXEOS chez
la femme enceinte. Il faut recommander aux patientes d’éviter de devenir
enceintes pendant un traitement par VYXEOS. Les hommes ainsi que les
femmes en âge de procréer doivent utiliser des méthodes contraceptives
efficaces pendant le traitement et pendant 6 mois après la dernière dose de
VYXEOS</li>
                <li>Selon les études menées chez les animaux, le traitement par VYXEOS
pourrait compromettre la fertilité masculine</li>
                <li>On doit recommander aux femmes de ne pas allaiter pendant le traitement
par VYXEOS</li>
            </ul>
            <h3 class="usage">Pour de plus amples renseignements :</h3>
            Consultez la monographie de VYXEOS, accessible à l’adresse <a href="https://www.jazzpharma.com">www.jazzpharma.com</a>, pour connaître les renseignements importants sur les
réactions indésirables, les interactions médicamenteuses et la posologie qui
n’ont pas été détaillés dans le présent document.
Il est également possible d’obtenir la monographie du produit en appelant
notre service médical, au : <a href="tel:+18005205568">1-800-520-5568</a>.
        </div>
    </div>
</div>

{% endblock %}