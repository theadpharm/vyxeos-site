{% extends "layout.nunjucks" %} {% block title %} Resources | Vyxeos {% endblock %} {% block content %}
<!--
  

-->
<style>
    .secondary-color {
        color: #72267A;
        font-weight: 600;
    }

    .gray-border {
        border-top: #CACACA 5px solid;
        border-bottom: #CACACA 5px solid;
    }

    .primary {
        color: #fff;
        background-color: #72267A;
    }

    .secondary {
        color: #000;
        background-color: #D5BED7;
    }

    .empty {
        background-color: initial;

    }

    .icon {
        height: 80px;
    }

    .strong {
        font-weight: bold;
    }

    .heading {
        font-weight: bold;
        color: #72267A;
        font-size: larger;
    }

    .alert-clinical {
        background-color: #5F267A;
        color: #fff;
        font-weight: bold;
    }

    h4 {
        color: #8D267E;
    }

    .bottom-border {
        border-bottom: solid 5px #CACACA;
    }
    .top-border {
        border-top: solid 5px #CACACA;
    }
</style>



<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>EXPLORE VYXEOS RESOURCES</h1>
        </div>
    </div>
    <div class="row my-4 bottom-border align-items-center">
        <div class="col-12 col-xl-6 my-3">
            <h4>The Canadian story behind VYXEOS</h4>
            <p>Learn more about the Canadian roots of VYXEOS – from
                its early beginning as a response for change and to the
                development of a new technology for AML treatment.</p>
            <a href="/assets/pdf/en/vyxeos_made_in_canada.pdf" class="btn btn-outline-purple">Download</a>
        </div>
        <div class="col-12 col-xl-6 my-3">
            <h4>Dosing and administration guide</h4>
            <p>A guide for dosing that provides comprehensive instructions
                for induction and consolidation, preparation and
                administration with VYXEOS.</p>
            <a href="/assets/pdf/en/vyxeos_dose_card.pdf" class="btn btn-outline-purple">Download</a>
        </div>
        <div class="col-12 col-xl-6 my-3">
            <h4>VYXEOS informational booklet</h4>
            <p>Learn more about the key aspects of VYXEOS, including
                its mechanism of action, clinical trial data and dosing.</p>
            <a href="/assets/pdf/en/vyxeos_information_booklet.pdf" class="btn btn-outline-purple">Download</a>
        </div>
        <div class="col-12 col-xl-6 my-3">
            <h4>Patient brochure</h4>
            <p>A booklet to provide your patients with information about
                their treatment with VYXEOS.</p>
            <a href="/assets/pdf/en/vyxeos_patient_brochure.pdf" class="btn btn-outline-purple">Download</a>
        </div>
        <div class="col-12 col-xl-6 my-3">
           <img class="image mx-auto d-block  my-3" alt="VYXEOS Starter Kit" src="/assets/img/en/FPO.png" />
        </div>
          <div class="col-12 col-xl-6  my-3">
            
            <h4 class="">Get Started with the VYXEOS Starter Kit!</h4>
            <p class="">Find these resources in the VYXEOS Starter Kit. Order your Kit by contacting a Jazz representative or by clicking <a href="contact_us.html">here</a> to reach Jazz Pharmaceuticals Canada, Inc.</p>
        </div>
    </div> 
    <div class="row">
        <div class="col-12">
            <h2>VIDEOS</h2>
        </div>
    </div>
    <div class="row my-4 ">
        <div class="col-12 col-xl-6 my-3">
            <h4>VYXEOS: Made in Canada</h4>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe title="Canadian story of VYXEOS video" src="https://player.vimeo.com/video/588422889?h=b4b7877fd1" allowfullscreen></iframe>
            </div>
            <a href="https://vimeo.com/588422889/b4b7877fd1" target="_blank" class="btn btn-outline-purple mt-3">Watch here</a>
        </div>
        <div class="col-12 col-xl-6 my-3">
            <h4>Mechanism of action</h4>
            <div class="embed-responsive embed-responsive-16by9">
                 <iframe title="MOA video of VYXEOS" src="https://player.vimeo.com/video/579950585?h=db58dcf229" allowfullscreen></iframe>
            </div>
            <a href="https://vimeo.com/579950585/db58dcf229" target="_blank" class="btn btn-outline-purple mt-3">Watch here</a>
        </div>
        <div class="col-12 col-xl-6 my-3">
            <h4>Preparation and handling instructions</h4>
            <div class="embed-responsive embed-responsive-16by9">
            <iframe title="Reconstituting VYXEOS video" src="https://player.vimeo.com/video/579951200?h=9bc1cdb1ef" allowfullscreen></iframe>
            </div>
            <a href="https://vimeo.com/579951200/9bc1cdb1ef" target="_blank" class="btn btn-outline-purple mt-3">Watch here</a>
        </div>

       
    </div>
   
</div>

{% endblock %}