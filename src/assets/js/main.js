
$(function() {
    $('map').imageMapResize();
    //$('[data-toggle="tooltip"]').tooltip()
    // $('a[target="_blank"], area[target="_blank"]').click(function(e) {
    //     e.preventDefault();

    //     var $theLink = $(this);
    //     var theLinkAddr = $theLink.attr('href');

    //     $('#modal-external-link').modal('show');
    //     $('#modal-external-link .btn-continue').click(function() {
    //         window.open( theLinkAddr );
    //     });
    // });

    var loginInfo = window.localStorage.getItem('login_info');
    console.log(window.location.pathname);
    if ( ! loginInfo && window.location.pathname !== '/en/login.html' && window.location.pathname !== '/fr/login.html' && window.location.pathname !== '/en/index.html' && window.location.pathname !== '/fr/index.html' ) {
      if(window.location.pathname.includes('/fr/')){
        window.location.href = '/fr/login.html?redirect='+window.location.pathname;
      }else {
        window.location.href = '/en/login.html?redirect='+window.location.pathname;
      }
    }

  

   /* 'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
    */
    var path = window.location.pathname.replace('/en/', '').replace('/fr/', '');
    console.log('the path: ', path);
    $('.nav-link[href="' + path + '"]').parent().addClass('active');
    $('.dropdown-item[href="' + path + '"]').addClass('active').parent().parent().addClass('active');
    $('.eng-lang').click(function() {
      window.location.pathname = window.location.pathname.replace('/fr/', '/en/');
    });
    $('.french-lang').click(function() {
      window.location.pathname = window.location.pathname.replace('/en/', '/fr/');
    });
    /**
     *   <div class="dropdown-menu">
                <a  class="dropdown-item eng-lang" href="#">ENGLISH</a>
                <a class="dropdown-item french-lang" href="#">FRANÇAIS</a>
              </div>
     * for dropdown inside dropdown in non-mobile nav
     */
    // $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
    //   if (!$(this).next().hasClass('show')) {
    //     $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
    //   }
    //   var $subMenu = $(this).next('.dropdown-menu');
    //   $subMenu.toggleClass('show');
    
    
    //   $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
    //     $('.dropdown-submenu .show').removeClass('show');
    //   });
    
    
    //   return false;
    // });

    // $('.contact-province-select').change(function() {
    //   var newValue = $(this).find('option:selected');
    //   var selected = $(newValue).data('target');
    //   $('.contact-info').addClass('hide');
    //   $('.' + selected).removeClass('hide');
    // });
})
